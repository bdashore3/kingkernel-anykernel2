#!/system/bin/sh

# (c) KingKernel kernel changes

sleep 30;

#change IO sched
echo "maple" > /sys/block/sda/queue/scheduler
echo "maple" > /sys/block/sdb/queue/scheduler
echo "maple" > /sys/block/sdc/queue/scheduler
echo "maple" > /sys/block/sdd/queue/scheduler
echo "maple" > /sys/block/sde/queue/scheduler
echo "maple" > /sys/block/sdf/queue/scheduler

#Set default gov to blu_schedutil (thanks eng.stk for the governor)
echo "blu_schedutil" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
echo "blu_schedutil" > /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
echo "blu_schedutil" > /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor
echo "blu_schedutil" > /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor

echo "Boot tweaks applied " > /dev/kmsg
